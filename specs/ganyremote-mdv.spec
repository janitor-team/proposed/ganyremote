%define name ganyremote
%define version 8.1
%define release %mkrel 1

Summary: GTK frontend for anyRemote
Name: %{name}
Version: %{version}
Release: %{release}
License: GPLv3+
Group: Applications/System
Requires: gtk3, python-pybluez >= 0.9.2, bluez >= 4.64, anyremote >= 6.7
BuildRequires: gettext, gettext-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildArch: noarch
Source0: %{name}-%{version}.tar.gz
URL: http://anyremote.sourceforge.net/

%description
gAnyRemote package is GTK GUI frontend for anyRemote 
(http://anyremote.sourceforge.net/) - remote control software for applications 
using Bluetooth or Wi-Fi.

%prep

%setup -q

%build
./configure --prefix=%{_prefix}

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall INSTALLDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_datadir}/appdata/ganyremote.appdata.xml
%{_datadir}/applications/ganyremote.desktop 
%{_datadir}/pixmaps/ganyremote_flash.png 
%{_datadir}/pixmaps/ganyremote_off.png 
%{_datadir}/pixmaps/ganyremote_light.png  
%{_datadir}/pixmaps/ganyremote.png
%{_defaultdocdir}/%{name}
%{_datadir}/locale/bg/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/cs_CZ/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/de_AT/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/de_DE/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/el_GR/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/es_ES/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/fr_FR/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/fi_FI/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/hu_HU/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/it_IT/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/nl_NL/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/pl_PL/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/pt_BR/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/ru_RU/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/sk_SK/LC_MESSAGES/ganyremote.mo
%{_datadir}/locale/sv_SE/LC_MESSAGES/ganyremote.mo

%changelog

* Wed Nov 11 2020 Mikhail Fedotov <anyremote at mail.ru> - 8.1
- Some fixes

* Fri Mar 21 2019 Mikhail Fedotov <anyremote at mail.ru> - 8.0
- Port ganyremote from from python2 to python3

* Tue Jan 30 2018 Mikhail Fedotov <anyremote at mail.ru> - 7.0
- Port ganyremote from PyGTK to PyGObject

* Sun Jan 11 2015 Mikhail Fedotov <anyremote at mail.ru> - 6.3.3
- Avahi support

* Fri Jul 11 2014 Mikhail Fedotov <anyremote at mail.ru> - 6.3.2
- Large application icon and AppData support.

* Mon Sep 16 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.3.1
- Greek translation was added (Thanks to Ioannis Servetas)

* Mon Aug 12 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.3
- Small correction.

* Mon Jun 10 2013 Mikhail Fedotov <anyremote at mail.ru> - 6.2
- Multiconnection and autostart support.

* Wed Oct 10 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.1
- Drop lightthpd dependency. Translation updates

* Mon Aug 13 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.0.1
- Translation update 

* Fri May 25 2012 Mikhail Fedotov <anyremote at mail.ru> - 6.0
- Update to work with anyremote v6.0, drop support of anyremote2html

* Sun Dec 4 2011 Mikhail Fedotov <anyremote at mail.ru> - 5.13
- Fix redhat bug 758414, fix to work properly with pygtk 2.10
  add --tray commandline option

* Fri Mar 11 2011 Mikhail Fedotov <anyremote at mail.ru> - 5.12
- Czech translation updated. Correctly works with anyRemote v5.4

* Wed Aug 4 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.6
- Fixed RedHat bugzilla bug 622589, do not use /sbin/ip if it absent

* Fri Jul 16 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.5
- Docs search path corrected.

* Tue Jul 6 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.4
- Small correction.

* Tue Mar 9 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.3
- Some correction in translations.

* Mon Feb 15 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.2
- Some correction in translations. 128x128 java client icons handling.

* Wed Jan 27 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11.1
- Small updates.

* Fri Jan 22 2010 Mikhail Fedotov <anyremote at mail.ru> - 5.11
- Command Fusion iViewer support.

* Mon Jul 6 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.10.2
- Translations were updated.

* Thu Jul 2 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.10
- Tool was rewritten on QT4. Enhanced handling of GuiAppBinary tag.
  Handle java client with 48x48 icons.

* Fri May 26 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.9
- Slovak translation was added (thanks to Michal Toth)

* Tue Apr 9 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8.2
- Small enhancements 

* Tue Apr 7 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8.1
- Fix small bug 

* Mon Mar 30 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.8
- Add GuiAppModes tag handling

* Wed Mar 11 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.7
- Finnish and Swedish translation were added (thanks to Matti Jokinen)

* Wed Jan 21 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.6.1-2
- correct spec

* Wed Jan 21 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.6.1-1
- Minor bugfix

* Mon Jan 19 2009 Mikhail Fedotov <anyremote at mail.ru> - 5.6-1
- Check java client version on the web site

* Sun Dec 21 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.5.1-1
- Fix upload from web feature

* Sun Dec 14 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.5-1
- Handle GuiAppVersion parameter in configuration files. Add possibility
  to download java client from Web. Small Ubuntu-specific fixes

* Wed Dec 3 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.4.1-1
- Fix detection of activity of bluetooth service

* Fri Oct 17 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.4-1
- Enhanced edit configuration file window. Support application details 
  auto wrap. Added Bulgarian translation (thanks to Stanislav Popov)

* Wed Sep 24 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.3-1
- Add icons to menu and buttons.

* Mon Sep 8 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.2.1-1
- Small bugfixes.

* Thu Sep 4 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.2-1
- Added "Details" field to the main window.
  Added French translation.

* Tue Aug 19 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.1-1
- Fixed crash on startup issue.
  Added Czech and  Dutch translations.

* Mon Jul 21 2008 Mikhail Fedotov <anyremote at mail.ru> - 5.0-1
- Internationalization support.
  Added Austrian, Brazilian Portuguese, German, Hungarian, Spanish, Italian, 
  Polish and Russian translation.

* Sun May 25 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.9-1
- Bugfixes and enhancements to better support anyremote-J2ME client v4.6 and
  anyremote2html v0.5.

* Sat Apr 20 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.8-1
- Some small enhancements. Spec file correction.

* Mon Mar 11 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.7-1
- Some small enhancements. Corrected to work properly with anyRemote v4.4.

* Tue Feb 26 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.6-2
- Spec file correction

* Sun Feb 20 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.6-1
- Handle absense of .anyRemote directory

* Sun Feb 17 2008 Mikhail Fedotov <anyremote at mail.ru> - 4.5-2
- Spec file correction
